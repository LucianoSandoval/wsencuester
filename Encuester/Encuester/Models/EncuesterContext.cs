﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Encuester.Models
{
    public partial class EncuesterContext : DbContext
    {
        public EncuesterContext()
        {
        }

        public EncuesterContext(DbContextOptions<EncuesterContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbEncuestum> TbEncuesta { get; set; }
        public virtual DbSet<TbOpcion> TbOpcions { get; set; }
        public virtual DbSet<TbPreguntum> TbPregunta { get; set; }
        public virtual DbSet<TbRespuestum> TbRespuesta { get; set; }
        public virtual DbSet<TbTipoPreguntum> TbTipoPregunta { get; set; }
        public virtual DbSet<TbUsuario> TbUsuarios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-TUJF4LG;UID=LucS;PWD=123456;Database=Encuester;Trusted_Connection=False;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<TbEncuestum>(entity =>
            {
                entity.HasKey(e => e.IdEncuesta)
                    .HasName("PK__TB_Encue__72579B54CD71223E");

                entity.ToTable("TB_Encuesta");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Encuesta)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.Link)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.Usuario)
                    .WithMany(p => p.TbEncuesta)
                    .HasForeignKey(d => d.UsuarioId)
                    .HasConstraintName("FK_UsuarioEncuesta");
            });

            modelBuilder.Entity<TbOpcion>(entity =>
            {
                entity.HasKey(e => e.IdOpcion)
                    .HasName("PK__TB_Opcio__4F238858FCCFF96D");

                entity.ToTable("TB_Opcion");

                entity.Property(e => e.Valor)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.Pregunta)
                    .WithMany(p => p.TbOpcions)
                    .HasForeignKey(d => d.PreguntaId)
                    .HasConstraintName("FK_PregOpc");
            });

            modelBuilder.Entity<TbPreguntum>(entity =>
            {
                entity.HasKey(e => e.IdPregunta)
                    .HasName("PK__TB_Pregu__754EC09E4C655981");

                entity.ToTable("TB_Pregunta");

                entity.Property(e => e.Pregunta)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.HasOne(d => d.Encuesta)
                    .WithMany(p => p.TbPregunta)
                    .HasForeignKey(d => d.EncuestaId)
                    .HasConstraintName("FK_EncPreg");

                entity.HasOne(d => d.Tipo)
                    .WithMany(p => p.TbPregunta)
                    .HasForeignKey(d => d.TipoId)
                    .HasConstraintName("FK_TipoPreg");
            });

            modelBuilder.Entity<TbRespuestum>(entity =>
            {
                entity.HasKey(e => e.Idrespuesta)
                    .HasName("PK__TB_Respu__F75909193AC605FC");

                entity.ToTable("TB_Respuesta");

                entity.Property(e => e.Idrespuesta).HasColumnName("IDRespuesta");

                entity.Property(e => e.Respuesta)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.Opcion)
                    .WithMany(p => p.TbRespuesta)
                    .HasForeignKey(d => d.OpcionId)
                    .HasConstraintName("FK_OpcResp");

                entity.HasOne(d => d.Pregunta)
                    .WithMany(p => p.TbRespuesta)
                    .HasForeignKey(d => d.PreguntaId)
                    .HasConstraintName("FK_PregResp");
            });

            modelBuilder.Entity<TbTipoPreguntum>(entity =>
            {
                entity.HasKey(e => e.IdTipo)
                    .HasName("PK__TB_TipoP__9E3A29A5FA1E533B");

                entity.ToTable("TB_TipoPregunta");

                entity.Property(e => e.Tipo)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TbUsuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario)
                    .HasName("PK__TB_Usuar__5B65BF97C84B790A");

                entity.ToTable("TB_Usuario");

                entity.HasIndex(e => e.Correo, "UQ__TB_Usuar__60695A1913AA9C85")
                    .IsUnique();

                entity.Property(e => e.Correo)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Pass)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
