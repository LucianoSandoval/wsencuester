﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Encuester.Models
{
    public partial class TbRespuestum
    {
        public int Idrespuesta { get; set; }
        public string Respuesta { get; set; }
        public int? OpcionId { get; set; }
        public int? PreguntaId { get; set; }

        public virtual TbOpcion Opcion { get; set; }
        public virtual TbPreguntum Pregunta { get; set; }
    }
}
