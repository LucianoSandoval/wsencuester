﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Encuester.Models
{
    public partial class TbEncuestum
    {
        public TbEncuestum()
        {
            TbPregunta = new HashSet<TbPreguntum>();
        }

        public int IdEncuesta { get; set; }
        public string Encuesta { get; set; }
        public string Descripcion { get; set; }
        public string Link { get; set; }
        public bool? Estatus { get; set; }
        public int? UsuarioId { get; set; }

        public virtual TbUsuario Usuario { get; set; }
        public virtual ICollection<TbPreguntum> TbPregunta { get; set; }
    }
}
