﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Encuester.Models
{
    public partial class TbPreguntum
    {
        public TbPreguntum()
        {
            TbOpcions = new HashSet<TbOpcion>();
            TbRespuesta = new HashSet<TbRespuestum>();
        }

        public int IdPregunta { get; set; }
        public string Pregunta { get; set; }
        public int? TipoId { get; set; }
        public int? EncuestaId { get; set; }

        public virtual TbEncuestum Encuesta { get; set; }
        public virtual TbTipoPreguntum Tipo { get; set; }
        public virtual ICollection<TbOpcion> TbOpcions { get; set; }
        public virtual ICollection<TbRespuestum> TbRespuesta { get; set; }
    }
}
