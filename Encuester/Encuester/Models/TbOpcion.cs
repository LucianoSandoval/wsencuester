﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Encuester.Models
{
    public partial class TbOpcion
    {
        public TbOpcion()
        {
            TbRespuesta = new HashSet<TbRespuestum>();
        }

        public int IdOpcion { get; set; }
        public string Valor { get; set; }
        public int? PreguntaId { get; set; }

        public virtual TbPreguntum Pregunta { get; set; }
        public virtual ICollection<TbRespuestum> TbRespuesta { get; set; }
    }
}
