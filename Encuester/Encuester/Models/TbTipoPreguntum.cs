﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Encuester.Models
{
    public partial class TbTipoPreguntum
    {
        public TbTipoPreguntum()
        {
            TbPregunta = new HashSet<TbPreguntum>();
        }

        public int IdTipo { get; set; }
        public string Tipo { get; set; }

        public virtual ICollection<TbPreguntum> TbPregunta { get; set; }
    }
}
