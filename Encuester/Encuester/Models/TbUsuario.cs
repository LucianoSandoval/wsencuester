﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Encuester.Models
{
    public partial class TbUsuario
    {
        public TbUsuario()
        {
            TbEncuesta = new HashSet<TbEncuestum>();
        }

        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public string Pass { get; set; }
        public bool? Estatus { get; set; }

        public virtual ICollection<TbEncuestum> TbEncuesta { get; set; }
    }
}
