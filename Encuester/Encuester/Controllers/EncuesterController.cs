﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Encuester.Models;
namespace Encuester.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Produces("application/json")]
    public class EncuesterController : Controller
    {
        EncuesterContext context = new EncuesterContext();
        public IActionResult Index()
        {
            return View();
        }
        #region Usuario
        [HttpGet]
        public IActionResult InicioSesion(string Correo, string Pass)
        {
            try
            {
                var Usuario = (from tUsuario in context.TbUsuarios where tUsuario.Correo == Correo && tUsuario.Pass == Pass && tUsuario.Estatus==true select tUsuario).FirstOrDefault();
               if(Usuario!=null)
                {
                    return Ok(Usuario);
                }
               else
                {
                    return Ok("El correo y la contraseña no coinciden con ningun usuario disponible.");
                }
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Registro([FromBody] TbUsuario tbUsuario)
        {
            try
            {
                context.TbUsuarios.Add(tbUsuario);
                context.SaveChanges();
                return Ok("Se ah registrado exitosamente.");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult EliminarUsuario(int Id)
        {
            try
            {
                var Usuario = (from tUsuario in context.TbUsuarios where tUsuario.IdUsuario == Id select tUsuario).FirstOrDefault();
                Usuario.Estatus = false;
                context.SaveChanges();
                return Ok("Se elimino exitosamente el usuario");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult EditarUsuario([FromBody] TbUsuario tbUsuario)
        {
            try
            {
                var Usuario = (from tUsuario in context.TbUsuarios where tUsuario.IdUsuario == tbUsuario.IdUsuario && tUsuario.Estatus==true select tUsuario).FirstOrDefault();
                if(tbUsuario.Nombre!=null && tbUsuario.Nombre!="" )
                {
                    Usuario.Nombre = tbUsuario.Nombre;  
                }
                if (tbUsuario.Correo != null && tbUsuario.Correo != "")
                {
                    Usuario.Correo = tbUsuario.Correo;
                }
                if (tbUsuario.Pass != null && tbUsuario.Pass != "")
                {
                    Usuario.Pass = tbUsuario.Pass;
                }
                context.SaveChanges();
                return Ok("Se edito exitosamente el usuario");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        #endregion
        #region Encuesta
        [HttpPost]
        public IActionResult CrearEncuesta([FromBody] TbEncuestum tbEncuestum)
        {
            try
            {
                context.TbEncuesta.Add(tbEncuestum);
                context.SaveChanges();
                return Ok("Se ah creado la encuesta exitosamente.");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult ConsultarEncuestas(int ID)
        {
            try
            {
                var Encuestas = (from tEncuesta in context.TbEncuesta where tEncuesta.UsuarioId==ID && tEncuesta.Estatus==true select tEncuesta).ToList();
                if (Encuestas != null)
                {
                    return Ok(Encuestas);
                }
                else
                {
                    return Ok("El usuario no tiene encuestas disponibles.");
                }
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult EliminarEncuesta(int ID)
        {
            try
            {
                var encuestum = (from tEncuesta in context.TbEncuesta where tEncuesta.IdEncuesta == ID select tEncuesta).FirstOrDefault();
                encuestum.Estatus = false;
                context.SaveChanges();
                return Ok("Se elimino exitosamente la encuesta");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult EditarEncuesta([FromBody] TbEncuestum tbEncuestum)
        {
            try
            {
                var Encuesta = (from tEncuesta in context.TbEncuesta where tEncuesta.IdEncuesta == tbEncuestum.IdEncuesta && tEncuesta.Estatus == true select tEncuesta).FirstOrDefault();
                if (tbEncuestum.Encuesta != null && tbEncuestum.Encuesta != "")
                {
                    Encuesta.Encuesta = tbEncuestum.Encuesta;
                }
                if (tbEncuestum.Descripcion != null && tbEncuestum.Descripcion != "")
                {
                    Encuesta.Descripcion = tbEncuestum.Descripcion;
                }
                if (tbEncuestum.Link != null && tbEncuestum.Link != "")
                {
                    Encuesta.Link = tbEncuestum.Link;
                }
                context.SaveChanges();
                return Ok("Se edito exitosamente la encuesta.");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }
        #endregion
        #region Pregunta
        [HttpGet]
        public IActionResult CATTipoPregunta()
        {
            try
            {
                var TipoPregunta = (from tTipoP in context.TbTipoPregunta select tTipoP).ToList();
                return Ok(TipoPregunta);
            }catch(Exception ex)
            {
                return Ok(ex.Message);
            }
        }
        [HttpPost]
        public IActionResult AgregaPregunta(TbPreguntum tbPreguntum)
        {
            try
            {
                context.TbPregunta.Add(tbPreguntum);
                context.SaveChanges();
                return Ok("Se agrego correctamente la pregunta");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }
        [HttpPost]
        public IActionResult EditarPregunta([FromBody] TbPreguntum tbPreguntum)
        {
            try
            {
                var Pregunta = (from tPrgunta in context.TbPregunta where tPrgunta.IdPregunta == tbPreguntum.IdPregunta select tPrgunta).FirstOrDefault();
                if (tbPreguntum.Pregunta != null && tbPreguntum.Pregunta != "")
                {
                    Pregunta.Pregunta = tbPreguntum.Pregunta;
                }
                if (tbPreguntum.EncuestaId != null && tbPreguntum.EncuestaId != 0)
                {
                    Pregunta.EncuestaId = tbPreguntum.EncuestaId;
                }
                context.SaveChanges();
                return Ok("Se edito exitosamente la pregunta.");
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpGet]
        public  IActionResult PreguntasEncuesta(int ID)
        {
            try
            {
                var Preguntas = (from tPreguntas in context.TbPregunta where tPreguntas.EncuestaId == ID select tPreguntas);
                return Ok(Preguntas);
            }catch(Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult EliminaPregunta(int ID)
        {
            try {
                var Pregunta = (from tPregunta in context.TbPregunta where tPregunta.IdPregunta==ID select tPregunta).FirstOrDefault();
                var Respuestas = (from tRespuesta in context.TbRespuesta where tRespuesta.PreguntaId==ID select tRespuesta).ToList();
                var Opciones = (from tOpc in context.TbOpcions where tOpc.PreguntaId == ID select tOpc).ToList();
                if (  Respuestas!=null ) { context.TbRespuesta.RemoveRange(Respuestas); }
                if (Opciones != null) { context.TbOpcions.RemoveRange(Opciones); }
                if (Pregunta != null) { context.TbPregunta.Remove(Pregunta); }
                context.SaveChanges();
                return Ok("Se elimino exitosamente la pregunta.");
            } catch (Exception ex){ 
                return Ok(ex.Message); }
        }
        #endregion
        #region Opciones
        [HttpPost]
        public IActionResult AgregarOpcion([FromBody] TbOpcion tbOpcion)
        {
            try
            {
                context.TbOpcions.Add(tbOpcion);
                context.SaveChanges();
                return Ok("Se agrego correctamente la opcion");
            }
            catch(Exception ex)
            {
                return Ok(ex.Message);
            }
        }
        [HttpPost]
        public IActionResult EditarOpcion([FromBody] TbOpcion tbOpcion)
        {
            try
            {
                var Opciones = (from tOpcion in context.TbOpcions where tOpcion.IdOpcion == tbOpcion.IdOpcion select tOpcion).FirstOrDefault();
                if (tbOpcion.Valor!=null && tbOpcion.Valor!="")
                {
                    Opciones.Valor = tbOpcion.Valor;
                }
                context.SaveChanges();
                return Ok("Se edito correctamente la opcion.");
            }catch(Exception ex)
            {
                return Ok(ex.Message);
            }
        }
        [HttpGet]
        public IActionResult OpcionesPregunta(int ID)
        {
            try
            {
                var Opciones = (from tOpcion in context.TbOpcions where tOpcion.PreguntaId == ID select tOpcion).ToList();
                return Ok(Opciones);
            }catch(Exception ex)
            {
                return Ok(ex.Message);
             }
        }
        #endregion
        #region Respuestas
        [HttpPost]
        public IActionResult AgregaRespuesta([FromBody] TbRespuestum tbRespuestum)
        {
            try
            {
                context.TbRespuesta.Add(tbRespuestum);
                context.SaveChanges();
                return Ok("Se agrego correctamente la respuesta.");
            }catch(Exception ex)
            {
                return Ok(ex.Message);
            }
        }
        [HttpPost]
        public IActionResult EditarRespuesta([FromBody] TbRespuestum tbRespuestum)
        {
            try
            {
                var Respuestas = (from tRespuestum in context.TbRespuesta where tRespuestum.Idrespuesta==tbRespuestum.Idrespuesta select tRespuestum).FirstOrDefault();
                if(tbRespuestum.Respuesta!=null)
                {
                    Respuestas.Respuesta = tbRespuestum.Respuesta;
                }
                Respuestas.OpcionId = tbRespuestum.OpcionId;
                context.SaveChanges();
                return Ok("Se edito exitosamente la respuesta.");
            }catch(Exception ex)
            {
                return Ok(ex.Message);
            }
        }


        #endregion

        #region Consultas 

        [HttpGet]
        public IActionResult ConsultaRespuestasEncuesta(int ID)
        {
            try {
                var Preguntas = (from tPregunta in context.TbPregunta where tPregunta.EncuestaId == ID select tPregunta).ToList();
                foreach(TbPreguntum preguntum in Preguntas)
                {
                    preguntum.TbRespuesta= (from tRespuestas in context.TbRespuesta where tRespuestas.PreguntaId==preguntum.IdPregunta select tRespuestas).ToList();
                }
                return Ok(Preguntas);
            } catch (Exception ex){ return Ok(ex.Message); }
        }
        [HttpGet]
        public IActionResult ConsultaEncuestaCompleta(int ID)
        {
            try
            {
                var Encuesta = (from tEncuesta in context.TbEncuesta where tEncuesta.IdEncuesta==ID select tEncuesta).FirstOrDefault();
                Encuesta.TbPregunta = (from tPregunta in context.TbPregunta where tPregunta.EncuestaId == Encuesta.IdEncuesta select tPregunta).ToList();
                foreach (TbPreguntum preguntum in Encuesta.TbPregunta)
                {
                    preguntum.TbOpcions = (from tOpc in context.TbOpcions where tOpc.PreguntaId == preguntum.IdPregunta select tOpc).ToList();
                }
                return Ok(Encuesta);
            }
            catch (Exception ex) { return Ok(ex.Message); }
        }
        #endregion
    }
}
